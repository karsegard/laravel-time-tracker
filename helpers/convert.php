<?php

function format_seconds_to_time_components($secs)
{
    $negative = false;
    if($secs <0 ){
        $secs = abs($secs);
        $negative = true;
    }
    $hours = (int) floor($secs / 3600);
    $minutes = (int) floor((int) ((int) $secs / 60) % 60);
    $seconds = (int) $secs % 60;

    $value = sprintf('%02d:%02d:%02d', $hours, $minutes, $seconds);

    if($negative){
        $value = "-".$value;
    }
    
    return $value;
}
