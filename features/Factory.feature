Feature: Factories
    Background: Customer
    Given the model class is "KDA\Laravel\TimeTracker\Models\Timer"

    Scenario: Model can be created without any attribute
        Given crafting a record 
        Then The record is not null

    Scenario: Can start Timer
        Given starting a timer
        Then The timer is not null

    Scenario: Can stop Timer
        Given starting a timer
        And waiting "1" seconds
        And stopping the timer
        Then The timer is not null
        And The timer "value" is not "0"

    