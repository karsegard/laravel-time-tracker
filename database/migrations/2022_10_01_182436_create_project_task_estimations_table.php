<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Laravel\TimeTracker\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ServiceProvider::getTableName('timers'), function (Blueprint $table) {
            $table->id();
            $table->enum('type',['estimation','timer','manual_time']);
            $table->integer('value')->default(0);
            $table->nullableNumericMorphs('timeable');
            $table->string('comment')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('stopped_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ServiceProvider::getTableName('timers'));
    }
};
