<?php

namespace KDA\Laravel\TimeTracker\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\TimeTracker\Models\Timer;

class TimerFactory extends Factory
{
    protected $model = Timer::class;

    public function definition()
    {
        return [
           
        ];
    }
}
