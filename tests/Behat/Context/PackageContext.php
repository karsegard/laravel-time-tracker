<?php

namespace KDA\Tests\Behat\Context;
use KDA\Laravel\TimeTracker\Models\Timer;

/**
 * Defines application features from the specific context.
 */
class PackageContext extends BaseContext
{
    use Concerns\Factory;
    use Concerns\Models;

    protected $timer;
     /**
     * @Given starting a timer
     */
    public function startingATimer()
    {
       $this->timer=  Timer::start();

    }
 /**
     * @Given stopping the timer
     */
    public function stoppingTheTimer()
    {
        $this->timer->stop();
    }
    /**
     * @Then The timer is not null
     */
    public function theTimerIsNotNull()
    {
        $this->assertNotNull($this->timer);
    }
    
     /**
     * @Given waiting :arg1 seconds
     */
    public function waitingSeconds($arg1)
    {
        sleep($arg1);
    }

    /**
     * @Then The timer :arg1 is not :arg2
     */
    public function theTimerIsNot($arg1, $arg2)
    {
        $this->assertTrue($this->timer->$arg1 != $arg2);
    }
}
