<?php

namespace KDA\Laravel\TimeTracker\Models;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use  KDA\Laravel\TimeTracker\Database\Factories\TimerFactory;
use KDA\Laravel\TimeTracker\Facades\TimeTracker;
use KDA\Laravel\TimeTracker\ServiceProvider;
class Timer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'type',
        'value',
        'timeable_type',
        'timeable_id',
        'comment',
        'user_id',
        'started_at',
        'stopped_at',
    ];

    protected $casts = [
        'started_at' => 'datetime',
        'stopped_at' => 'datetime',
    ];

    public function getTable()
    {
        return ServiceProvider::getTableName('timers');
    } 
    
    protected static function newFactory()
    {
        return  TimerFactory::new();
    }

    public function timeable()
    {
        return $this->morphTo();
    }
    

    public function stop()
    {
        $this->stopped_at = Carbon::now();
        $this->value = $this->started_at->diffInSeconds($this->stopped_at);
        $this->save();
    }

    public static function start($estimatable=null)
    {
        return TimeTracker::start($estimatable);
    }

    public function getValueAttribute(){
        if(blank($this->stopped_at)){
            return $this->started_at->diffInSeconds(now());
        }
        return $this->attributes['value'];
    }

    public function getHumanValueAttribute(){
        return CarbonInterval::seconds($this->value)->cascade()->forHumans();
    }

    public function setStoppedAtAttribute($value){
        if(!blank($value)){
            $this->value = $this->started_at->diffInSeconds($value);
        }
        $this->attributes['stopped_at']=$value;
    }

    public function scopeForTimeableType($q,$type){
        return $q->where('timeable_type',$type);
    }
    public function scopeForTimeable($q,$timeable){
        return $q->where('timeable_id',$timeable->getKey())->forTimeableType(get_class($timeable));
    }
}
