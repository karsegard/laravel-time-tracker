<?php 
namespace KDA\Laravel\TimeTracker\Models\Traits;

use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\TimeTracker\Facades\TimeTracker;

trait Timeable{


    public function startTimer(){
        TimeTracker::start($this);
    }

    public function timers(){
        $class =TimeTracker::getModelClass();
        return $this->morphMany($class, 'timeable');
    }

    public function scopeWithRunningTimers($q){
        return $q->whereHas('timers',function($q){
            $q->whereNull('stopped_at');
        });
    }

    public function getHasRunningTimerAttribute(){
        return $this->running_timers->count()>0;
    }

    public function getRunningTimersAttribute(){
        return $this->timers->whereNull('stopped_at');
    }

    public function getRunningTimerForUser(Model $user){
        return $this->running_timers->where('user_id',$user->getKey());
    }
    public function getRunningTimerForOtherUserThan(Model $user){
        return $this->running_timers->where('user_id','!=',$user->getKey());
    }
    public function hasRunningTimerForOtherUserThan(Model $user){
        return $this->getRunningTimerForOtherUserThan($user)->count()>0;
    }
    public function hasRunningTimerForUser(Model $user){
        return $this->getRunningTimerForUser($user)->count()>0;
    }

    public function getTotalTrackedTimeAttribute(){
        return $this->timers->sum('value');
    }
    public function getTotalTrackedTimeHumanAttribute(){
        return CarbonInterval::seconds($this->total_tracked_time)->cascade()->forHumans();
    }

    public function toHumanTime($time,$null='-'){
        return !blank($time) ? CarbonInterval::seconds($time)->cascade()->forHumans() : $null;
    }
  
}
