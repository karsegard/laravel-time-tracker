<?php
namespace KDA\Laravel\TimeTracker;

use Carbon\Carbon;
use KDA\Laravel\TimeTracker\Models\Timer;

//use Illuminate\Support\Facades\Blade;
class TimeTracker 
{

    public function getModelClass(){
        return Timer::class;
    }

    public function start($estimatable=null)
    {
        $class =$this->getModelClass();
        $model = new $class();
        $model->fill([
            'user_id' => auth()?->user()?->getKey(),
            'type' => 'timer',
            'started_at' => Carbon::now(),]);
        if($estimatable){
            $model->timeable()->associate($estimatable);
        }
        $model->save();
        return $model;
    }

    public function addTime($estimatable,$sec){
        $class =$this->getModelClass();
        $model = new $class();
        $model->fill([
            'user_id' => auth()?->user()?->getKey(),
            'type' => 'manual_time',
            'started_at' => Carbon::now(),
            'stopped_at' => Carbon::now()->addSeconds($sec),
        ]);
        if($estimatable){
            $model->timeable()->associate($estimatable);
        }
        $model->save();
        return $model;
    }

    public function stop($timeable)
    {
        $timer= $timeable->load('timers')->timers->whereNull('stopped_at')->where('user_id',auth()->user()->getKey());
        if($timer){
            $timer->each->stop();
        }
        return $timer;
    }

    public function hasStartedTimer($timeable){
        return $timeable->load('timers')->timers->whereNull('stopped_at')->get()->count()>0;
    }
}