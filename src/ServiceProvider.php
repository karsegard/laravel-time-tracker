<?php
namespace KDA\Laravel\TimeTracker;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\TimeTracker\Facades\TimeTracker as Facade;
use KDA\Laravel\TimeTracker\TimeTracker as Library;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasConfigurableTableNames;
use KDA\Laravel\Traits\HasDumps;
use KDA\Laravel\Traits\HasHelper;
use KDA\Laravel\Traits\HasLoadableMigration;
use KDA\Laravel\Traits\HasMigration;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasMigration;
    use HasLoadableMigration;
    use HasDumps;
    use HasConfigurableTableNames;
    use HasHelper;
    
    protected $packageName ='laravel-time-tracker';
    protected $configDir='config';
    protected $configs = [
         'kda/time-tracker.php'  => 'kda.time-tracker'
    ];
    protected static $tables_config_key="kda.time-tracker.tables";
    public function getDumps():array{
        return config(self::$tables_config_key);
    }
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
